# Requirements
Python >3.8 is required to run this module.
See [this post][11] to enforce version with packaging tools.

# Learning Scrapy
## Useful links
- [Scrapy tutorial][1]: official scrapy tutorial
- [XPath tutorial][2]: XML Path Language is a query language for selecting nodes from an XML document. It works with Scrapy selectors.
- [XPath reference][3]: 35 pages XPath reference document
- [XPath cheatsheet][4]: Xpath cheatsheet
- [XPath test bed][6]: A place to test XPath expressions
- [Post request][5]: Scrapy POST request example

# Scrapy debugging

To inspect the response in a browser:

```
from scrapy.shell import inspect_response
    inspect_response(response, self)
```

To write response text using appropriate encoding
```
with open('response_text.html', 'w', encoding='windows-1252') as file:
    file.write(response.text)
```


# Scrapy testing
## Unit testing
- [Fake Request/response with files][7]

# Using Splash for Javascript
Splash is useful to get html content that is rendered from JavaScript. I.e. the ArkoVision image viewer top bar.

Need to install Docker and get a Splash docker container. See [Handling JavaScript in Scrapy with Splash][10].

To run the Splash Docker container:
```
$ docker run -p 5023:5023 -p 8050:8050 -p 8051:8051 scrapinghub/splash
```

# Getting Archives elements
## Scrapy `FormRequest` parameters
When issuing a `FormRequest` to get a municipality data, the response is redirected to the same URL.
This imposes following parameters to be used:
- `meta={'cookiejar': i}` with a distinct `cookiejar` for each municipality.
- `dont_filter=True` to avoid Scrapy filtering identical requests.

## Access to TD (Tables décennales)
For Aube department, there is a dedicated entry page.
This is not the case for Ardennes where civil registry and TDs are gathered.

For large municipalities, TDs entries can be spread on several pages (i.e. for Troyes). It is required to crawl the
TD list on several pages by following links.

**For Loiret, table of tables décennales links may contain 6 or 7 columns depending
on the existence of some notes.**

## Permalinks (Aube)

Permalinks are in

```<img class="imgThumb_img" id="thumb1" src="spacer.gif" data-src="imgThumb.php?h=150&amp;c=010bde1a47&amp;i=W1BST1RdY2dhdWJlL2ltYWdlcy9kZWNlbm5hbGVzL0FEMDEwXzAwMDAxM19FXzEvU1JDL0FEMDEwXzVNSTAxM18wMDcwNV9DLmpwZw==" data-title="Romilly-sur-Seine 1792 1802 " data-cote="" data-arklink="http://www.archives-aube.fr/ark:/42751/s0055682249d04a8/55682249d05fd">```

elements. Those elements are loaded in Google Chrome
when using a link of the type [http://www.archives-aube.fr/ + scraped image link](http://www.archives-aube.fr/arkotheque/arkotheque_visionneuse_archives.php?arko=YTo0OntzOjQ6ImRhdGUiO3M6MTA6IjIwMjEtMDUtMTIiO3M6MTA6InR5cGVfZm9uZHMiO3M6MTE6ImFya29fc2VyaWVsIjtzOjQ6InJlZjEiO2k6MTtzOjQ6InJlZjIiO2k6MjcwNzt9).

To get the permalinks for Aube use following code:
```
response.xpath('//img[@class="imgThumb_img"]/@data-arklink').getall())
```
on the response from the first page of the image viewer.

Then look at [How do I merge results from target page to current page in scrapy?][8] and 
[Passing additional data to callback functions][9] to collect permalinks into a single item.


# TODOs
- Deal with La, Le, Les in municipalities


[1]: https://docs.scrapy.org/en/latest/intro/tutorial.html
[2]: http://zvon.org/comp/r/tut-XPath_1.html
[3]: http://www.utools.nl/downloads/XPathReference.pdf
[4]: https://devhints.io/xpath
[5]: https://gist.github.com/arun-shaji/895df5216ff6905f70228cb1ab228636
[6]: http://www.whitebeam.org/library/guide/TechNotes/xpathtestbed.rhtm
[7]: https://stackoverflow.com/questions/6456304/scrapy-unit-testing
[8]: https://stackoverflow.com/questions/8467700/how-do-i-merge-results-from-target-page-to-current-page-in-scrapy
[9]: https://docs.scrapy.org/en/latest/topics/request-response.html#passing-additional-data-to-callback-functions
[10]: https://www.zyte.com/blog/handling-javascript-in-scrapy-with-splash/
[11]: https://stackoverflow.com/questions/6224736/how-to-write-python-code-that-is-able-to-properly-require-a-minimal-python-versi/6224814