from ast import literal_eval
from json import loads
from pathlib import Path
from typing import Callable
from urllib.parse import urlencode, urlparse, urlunparse

DEPARTMENTS_MUNICIPALITIES = Path(__file__).resolve(strict=True).parent / 'indata/departments-municipalities.jl'
DEPARTMENTS_TD_PROXIES = Path(__file__).resolve(strict=True).parent / 'indata/departments-td-proxies.jl'


def build_municipality_query_url(raw_url: str, municipality_query_string: str, query_municipality_field: str) \
        -> Callable[[str], str]:
    """
    Closure for a municipality_query_url
    :param raw_url: url without the query string
    :param municipality_query_string: str representing the query
    :param query_municipality_field: str the key of the municipality to search for
    """
    query = literal_eval(municipality_query_string)

    def municipality_query_url(municipality: str):
        query[query_municipality_field] = municipality
        return urlunparse(urlparse(raw_url)._replace(query=urlencode(query, encoding='windows-1252')))

    return municipality_query_url


class TabDenProxy:

    @staticmethod
    def dpt_dict(fp: Path, dpt_code: str) -> dict:
        """
        Retrieve data relative to a department in a file
        :param fp: file path of the file
        :param dpt_code: the code of the department, i.e. 10 for Aube
        :return: dictionary containing the required data
        """
        with open(fp, 'r') as f:
            dpt_found = False
            for line in f:
                dpt_dict = loads(line)
                if dpt_dict['code'] == dpt_code:
                    dpt_found = True
                    break
        return dpt_dict if dpt_found else None

    @staticmethod
    def build_municipality_form(municipality_form_string: str, form_search_field: str):
        form = literal_eval(municipality_form_string)

        def municipality_form(municipality: str):
            form[form_search_field] = municipality
            return form

        return municipality_form

    def __init__(self, dpt_code: str):
        dpt_dict = self.dpt_dict(DEPARTMENTS_MUNICIPALITIES, dpt_code)
        if dpt_dict:
            self.dpt_code = dpt_dict['code']
            self.dpt_name = dpt_dict['name']
            self.municipalities = dpt_dict['municipalities']
        else:
            raise NotImplementedError("Department municipalities not found")

        dpt_dict = self.dpt_dict(DEPARTMENTS_TD_PROXIES, dpt_code)
        if dpt_dict:
            self.td_url = dpt_dict['td_url']
            self.td_rows_xpath = dpt_dict['td_rows_xpath']
            self.municipality_form = self.build_municipality_form(dpt_dict['municipality_form'],
                                                                  dpt_dict['form_search_field']) if dpt_dict[
                'municipality_form'] else None
        else:
            raise NotImplementedError("Department td proxy not found")
