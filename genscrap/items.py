# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from re import search
from scrapy import Item, Field
from itemloaders.processors import TakeFirst


def get_num_field(value):
    return search(r'(\d+)', value[0]).group(1) if search(r'(\d+)', value[0]) else ''


def get_striped(values):
    return values[0].strip(' \n') if values else ''


class TabDenLineItem(Item):
    municipality_details = Field(input_processor=get_striped, output_processor=TakeFirst())
    cote = Field(input_processor=get_striped, output_processor=TakeFirst())
    date_first = Field(input_processor=get_num_field, output_processor=TakeFirst())
    date_last = Field(input_processor=get_num_field, output_processor=TakeFirst())

    # TODO: look at forcing serialization of empty extra_data field.
    #  See https://docs.scrapy.org/en/latest/topics/exporters.html#scrapy.exporters.BaseItemExporter.export_empty_fields
    extra_data = Field(input_processor=get_striped, output_processor=TakeFirst())
    images_plinks = Field()


class MunicipalitiesItem(Item):
    department = Field(output_processor=TakeFirst())
    municipalities = Field()
