from urllib.parse import urlparse, parse_qs

from scrapy import Spider, Request, FormRequest
from scrapy.loader import ItemLoader

from ..items import MunicipalitiesItem


class Muni10Spider(Spider):
    name = "muni-10"

    def start_requests(self):
        urls = [
            'http://www.archives-aube.fr/s/1/tables-decennales/?',
        ]
        for url in urls:
            yield Request(url=url, callback=self.parse, encoding='windows-1252')

    # noinspection PyMethodOverriding
    def parse(self, response):
        url_get_list = response.urljoin(response.xpath('//a[@class="fiframe"]/@href').get())
        # noinspection PyTypeChecker
        yield response.follow(url=url_get_list,
                              callback=self.parse_get_list, encoding='windows-1252',
                              cb_kwargs=dict(arko=parse_qs(urlparse(url_get_list).query)['arko'][0]))

    # noinspection PyUnusedLocal
    def parse_get_list(self, response, arko):
        yield FormRequest(url='http://www.archives-aube.fr/arkotheque/arkotheque_liste_aide.php',
                          callback=self.parse_list, method='POST', encoding='windows-1252',
                          formdata={'action': '1', 'todo': 'choix_lettre', 'arko': arko, 'LaLettre': '*'})

    @staticmethod
    def parse_list(response):
        muni_loader = ItemLoader(item=MunicipalitiesItem(), response=response)
        muni_loader.add_value('department', '10')
        muni_loader.add_xpath('municipalities', '//select[@name="ChoixLettre"]/option/text()')

        yield muni_loader.load_item()


class Muni45Spider(Spider):
    name = "muni-45"

    def start_requests(self):
        urls = [
            'https://consultation.archives-loiret.fr/e/EtatCivil?from=0&f_11%5B0%5D=Tables+d%E9cennales',
        ]
        for url in urls:
            yield Request(url=url, callback=self.parse, encoding='windows-1252')

    # noinspection PyMethodOverriding
    def parse(self, response):
        muni_loader = ItemLoader(item=MunicipalitiesItem(), response=response)
        muni_loader.add_value('department', '45')
        muni_loader.add_xpath('municipalities', '//div[contains(@class, "ef_filtre_10")]//option/text()',
                              lambda muni_list_raw: [municipality[: municipality.rfind(' (')] for municipality in
                                                     muni_list_raw])

        yield muni_loader.load_item()
