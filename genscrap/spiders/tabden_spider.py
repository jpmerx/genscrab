from pathlib import Path
from re import search

from scrapy import Spider, FormRequest, Request
from scrapy.exceptions import CloseSpider
from scrapy.loader import ItemLoader

from ..departments import TabDenProxy, build_municipality_query_url
from ..items import TabDenLineItem

# noinspection PyPep8Naming
BASE_DIR = Path(__file__).resolve(strict=True).parent.parent


class TabDen10Spider(Spider):
    name = "td-10"

    def __init__(self, *args, department=None, **kwargs):
        super(TabDen10Spider, self).__init__(*args, **kwargs)
        try:
            self.td_proxy = TabDenProxy(department)
        except NotImplementedError:
            raise CloseSpider(reason="Department not implemented yet")
        # TODO: for Aube department, encoding has to be 'windows-1252'.
        #  Is it the same for other French departments? See:
        #  https://stackoverflow.com/questions/18359007/trying-to-get-encoding-from-a-webpage-python-and-beautifulsoup

    def start_requests(self):

        for i, municipality in enumerate(self.td_proxy.municipalities):
            yield FormRequest(url=self.td_proxy.td_url, callback=self.parse, method='POST', dont_filter=True,
                              formdata=self.td_proxy.municipality_form(municipality), encoding='windows-1252',
                              meta={'cookiejar': i})

    @staticmethod
    def has_next(response):
        x = response.xpath('//div[@id="abecedaire"]/a[text() = ">"]')
        if x:
            return x[0].xpath('@href').get()

    # noinspection PyMethodOverriding
    def parse(self, response):

        def images_link(value):
            # Get the quoted string inside the string
            return search(r'\'([^\']*)\'', value).group(1)

        for row in response.xpath(self.td_proxy.td_rows_xpath):
            tabden_first_loader = ItemLoader(item=TabDenLineItem(), selector=row)

            tabden_first_loader.add_xpath('municipality_details', 'child::*[1]/text()')
            tabden_first_loader.add_xpath('date_first', 'child::*[2]/text()')
            tabden_first_loader.add_xpath('date_last', 'child::*[3]/text()')
            tabden_first_loader.add_xpath('extra_data', 'child::*[4]/text()')

            images_js = row.xpath('child::*[5]').xpath('child::*[1]').css('a::attr(href)').get()

            yield Request(url=response.urljoin(images_link(images_js)),
                          callback=self.parse_plinks, encoding='windows-1252',
                          cb_kwargs=dict(tabden_line_partial=tabden_first_loader.load_item()),
                          meta={'cookiejar': response.meta['cookiejar']}, )

        next_url = self.has_next(response)
        if next_url:
            yield response.follow(url=response.urljoin(next_url), callback=self.parse, encoding='windows-1252',
                                  meta={'cookiejar': response.meta['cookiejar']}, )

    # noinspection PyMethodMayBeStatic
    def parse_plinks(self, response, tabden_line_partial):
        tabden_global_loader = ItemLoader(item=tabden_line_partial, response=response)
        tabden_global_loader.add_xpath('cote', '//img[@class="imgThumb_img"]/@data-cote', lambda x: x[0])
        tabden_global_loader.add_xpath('images_plinks', '//img[@class="imgThumb_img"]/@data-arklink')

        yield tabden_global_loader.load_item()


class TabDen45Spider(Spider):
    name = "td-45"

    def __init__(self, *args, department=None, **kwargs):
        super(TabDen45Spider, self).__init__(*args, **kwargs)
        try:
            self.td_proxy = TabDenProxy(department)
        except NotImplementedError:
            raise CloseSpider(reason="Department not implemented yet")
        self.municipality_query_url = build_municipality_query_url(
            'https://consultation.archives-loiret.fr/e/EtatCivil',
            "{'from': '0', 'f_11[0]': 'Tables décennales', 'f_10[0]': 'municipality'}",
            'f_10[0]')

    def start_requests(self):

        for i, municipality in enumerate(self.td_proxy.municipalities):
            yield Request(url=self.municipality_query_url(municipality), callback=self.parse, encoding='windows-1252',
                          meta={'cookiejar': i})

    @staticmethod
    def has_next(response):
        x = response.xpath('//div[@id="abecedaire"]/a[text() = ">"]')
        if x:
            return x[0].xpath('@href').get()

    # noinspection PyMethodOverriding
    def parse(self, response):

        def images_link(value):
            # Get the quoted string inside the string
            return search(r'\'([^\']*)\'', value).group(1)

        for row in response.xpath(self.td_proxy.td_rows_xpath):
            tabden_first_loader = ItemLoader(item=TabDenLineItem(), selector=row)

            tabden_first_loader.add_xpath('cote', 'child::*[1]/text()')
            tabden_first_loader.add_xpath('municipality_details', 'child::*[2]/text()')
            tabden_first_loader.add_xpath('extra_data', 'child::*[3]/text()')
            tabden_first_loader.add_xpath('date_first', 'child::*[5]/text()')
            tabden_first_loader.add_xpath('date_last', 'child::*[6]/text()')

            yield tabden_first_loader.load_item()
