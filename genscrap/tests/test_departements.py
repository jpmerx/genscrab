from ..departments import build_municipality_query_url


def test_build_municipality_query_url():
    raw_url = 'https://consultation.archives-loiret.fr/e/EtatCivil'
    municipality_query_string = "{'from': '0', 'f_11[0]': 'Tables décennales', 'f_10[0]': 'municipality'}"
    query_municipality_field = 'f_10[0]'
    municipality_query_url = build_municipality_query_url(raw_url, municipality_query_string, query_municipality_field)

    municipality = "Ferrières-en-Gâtinais"
    assert (municipality_query_url(municipality) ==
            'https://consultation.archives-loiret.fr/e/EtatCivil?'
            'from=0&f_11%5B0%5D=Tables+d%E9cennales&f_10%5B0%5D=Ferri%E8res-en-G%E2tinais')

    municipality = "Bussière (La)"
    assert (municipality_query_url(municipality) ==
            'https://consultation.archives-loiret.fr/e/EtatCivil?'
            'from=0&f_11%5B0%5D=Tables+d%E9cennales&f_10%5B0%5D=Bussi%E8re+%28La%29')
