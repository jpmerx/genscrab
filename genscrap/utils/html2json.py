from json import dump

from scrapy import Selector


def main():
    # !!! html file has to be windows-1252 encoded if it is the encoding of the archive website !!!
    with open("/home/jpmerx/dev/genscrap/genscrap/indata/loiret-municipalities.html",
              encoding='windows-1252') as infile:
        text = infile.read()
    muni_list_raw = Selector(text=text).xpath('//option/text()').getall()
    muni_list = [municipality[: municipality.rfind(' (')] for municipality in muni_list_raw]

    muni_json = {'department': 45, 'municipalities': muni_list}
    with open('/home/jpmerx/dev/genscrap/genscrap/indata/loiret-municipalities.json', 'w') as outfile:
        dump(muni_json, outfile)


if __name__ == "__main__":
    main()
